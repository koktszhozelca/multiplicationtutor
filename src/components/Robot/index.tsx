import React, { PureComponent } from 'react';
import './index.css'
import Paper from '../Paper';
import Grid from '../Grid';

import moment from 'moment';
import IconButton from '../IconButton';
import Speech from '../Speech';

class Robot extends PureComponent<any, {}> {
  constructor(props: any) {
    super(props);
    Speech.speak("Robot management tab is opened.");
  }

  render() {
    return (
      <div>
        <div className="form-header">
          <div className="form-title-floating">
            Robot Management
          </div>
          <div className="form-banner robot"></div>
        </div>
        <div className="form-content">
          <Paper
            title={
              <div>
                <div>
                  <span>Status</span>
                  <span style={{ float: "right", color: "green" }}>Connected</span>
                </div>
                <br />
                <div>
                  <span>Current Version</span>
                  <span style={{ float: "right" }}>0.0.5</span>
                </div>
              </div>
            }
            content={
              <table className="form-table">
                <thead>
                  <tr>
                    <td>Robot Database</td>
                    <td className="right">Actions</td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>0.0.5</td>
                    <td className="right disabled">
                      <IconButton label="Upload" icon="cloud_upload" />
                    </td>
                  </tr>
                  <tr>
                    <td>0.0.4</td>
                    <td className="right">
                      <IconButton label="Upload" icon="cloud_upload" />
                    </td>
                  </tr>
                  <tr>
                    <td>0.0.3</td>
                    <td className="right">
                      <IconButton label="Upload" icon="cloud_upload" />
                    </td>
                  </tr>
                  <tr>
                    <td>0.0.2</td>
                    <td className="right">
                      <IconButton label="Upload" icon="cloud_upload" />
                    </td>
                  </tr>
                  <tr>
                    <td>0.0.1</td>
                    <td className="right">
                      <IconButton label="Upload" icon="cloud_upload" />
                    </td>
                  </tr>
                </tbody>
              </table>
            } />
          <br />
          <Paper
            title={
              <div>
                <span>History</span>
                <span style={{ float: "right" }}>Current Version: 0.0.5</span>
              </div>
            }
            content={
              <table className="form-table">
                <thead>
                  <tr>
                    <td>Revised Version</td>
                    <td className="right">Creation Time</td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>0.0.5</td>
                    <td className="right">{moment().subtract(1, "day").format("YYYY-MM-DD HH:mm:ss")}</td>
                  </tr>
                  <tr>
                    <td>0.0.4</td>
                    <td className="right">{moment().subtract(2, "day").subtract(10, "minutes").format("YYYY-MM-DD HH:mm:ss")}</td>
                  </tr>
                  <tr>
                    <td>0.0.3</td>
                    <td className="right">{moment().subtract(3, "day").subtract(16, "hours").format("YYYY-MM-DD HH:mm:ss")}</td>
                  </tr>
                  <tr>
                    <td>0.0.2</td>
                    <td className="right">{moment().subtract(4, "day").subtract(8, "hours").format("YYYY-MM-DD HH:mm:ss")}</td>
                  </tr>
                  <tr>
                    <td>0.0.1</td>
                    <td className="right">{moment().subtract(5, "day").subtract(55, "minutes").format("YYYY-MM-DD HH:mm:ss")}</td>
                  </tr>
                </tbody>
              </table>
            } />
        </div>
      </div>
    )
  }
}

export default Robot;