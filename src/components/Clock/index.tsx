import React, { PureComponent } from 'react';
import "./index.css";
import moment from 'moment';

interface Props {
    //Props here
    second: number,
    minute: number,
    hour: number
}

interface State {
    //State here
    second: number,
    minute: number,
    hour: number
}

class Clock extends PureComponent<Props, State> {
    public static defaultProps = {
        second: -1,
        minute: -1,
        hour: -1
    }

    public state: State = {
        second: 0,
        minute: 0,
        hour: 0
    }

    componentDidMount() {
        this.animate();
    }

    private animate(isPropVal: boolean = true) {
        let hour = this.toDeg(isPropVal ? this.props.hour : this.state.hour, 12, 12);
        let minute = this.toDeg(isPropVal ? this.props.minute : this.state.minute, 60, 60);
        let second = this.toDeg(isPropVal ? this.props.second : this.state.second, 60, 60);
        minute += second / 360;
        hour += minute / 360 * 12;
        this.setState({ hour, minute, second });
    }

    private toDeg(time: number, totalTimeSlot: number, defaultVal: number) {
        let deg = (time ? time : defaultVal) / totalTimeSlot * 360;
        return deg === 360 ? 0 : deg;
    }

    setTime(hour: number, minute: number, second: number) {
        this.setState({ hour, minute, second }, () => this.animate(false));
    }

    render() {
        return (
            <div className="clock-base">
                <div className="clock-frame"></div>
                <div className="clock-center"></div>
                <div className="clock-second" style={{ transform: `rotate(${this.state.second}deg)` }}></div>
                <div className="clock-minute" style={{ transform: `rotate(${this.state.minute}deg)` }}></div>
                <div className="clock-hour" style={{ transform: `rotate(${this.state.hour}deg)` }}></div>
                <div className="clock-num-12">12</div>
                <div className="clock-num-1">1</div>
                <div className="clock-num-2">2</div>
                <div className="clock-num-3">3</div>
                <div className="clock-num-4">4</div>
                <div className="clock-num-5">5</div>
                <div className="clock-num-6">6</div>
                <div className="clock-num-7">7</div>
                <div className="clock-num-8">8</div>
                <div className="clock-num-9">9</div>
                <div className="clock-num-10">10</div>
                <div className="clock-num-11">11</div>
            </div>
        )
    }
}

export default Clock;