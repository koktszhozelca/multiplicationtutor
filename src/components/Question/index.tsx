import React, { PureComponent } from 'react';
import './index.css'
import Grid from '../Grid';
import Paging from '../Paging';
import Speech from '../Speech';

interface State {
    questStr: any
}

class Question extends PureComponent<any, {}> {
    public state: State = {
        questStr: null
    }
    private options: React.RefObject<Grid> = React.createRef();

    constructor(props: any) {
        super(props);
        this.state.questStr = props.questStr;        
    }

    componentDidMount() {
        let options = [
            { custom: <div className="question-option">1</div> },
            { custom: <div className="question-option">2</div> },
            { custom: <div className="question-option">3</div> },
            { custom: <div className="question-option">4</div> },
            { custom: <div className="question-option">5</div> },
            { custom: <div className="question-option">6</div> },
            { custom: <div className="question-option">7</div> },
            { custom: <div className="question-option">8</div> },
            { custom: <div className="question-option">9</div> }
        ];
        this.options.current!.addAll(options);
    }

    render() {
        return (
            <div className="question-container">
                <div className="form-title">
                    Question
                    <span style={{ float: "right" }}>( 1 / 10 )</span>
                </div>
                <div className="question-title">{this.state.questStr}</div>
                <div className="question-options-frame">
                    <Grid ref={this.options} handler={this} cardStyle={{ width: "90px", height: "90px", borderRadius: "100%" }} />
                </div>
                <Paging />
            </div>
        )
    }
}

export default Question;