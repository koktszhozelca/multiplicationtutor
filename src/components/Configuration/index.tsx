import React, { PureComponent } from 'react';
import './index.css'
import Paper from '../Paper';
import Grid from '../Grid';
import IconButton from '../IconButton';
import { ENGINE_METHOD_DIGESTS } from 'constants';
import { timingSafeEqual } from 'crypto';
import Speech from '../Speech';

interface State {
  members: string[]
}

class Configuration extends PureComponent<any, {}> {
  public state: State = {
    members: new Array<string>()
  }

  constructor(props: any) {
    super(props);
    Speech.speak("Configuration tab is opened.");
  }

  componentDidMount() {
    let members = [
      "Alice", "Zelca"
    ]
    this.setState({ members });
  }

  addMember = () => {
    let name: string | any = prompt("Please enter the new member's name:");
    this.state.members.push(name);
    this.forceUpdate();
  }

  render() {
    return (
      <div>
        <div className="form-header">
          <div className="form-title-floating" style={{ color: "black" }}>
            Configuration
          </div>
          <div className="form-banner config"></div>
        </div>
        <div className="form-content">
          <Paper
            title={
              <div>
                <span>Group Information</span>
                <span style={{ float: "right" }}>Group 13</span>
              </div>
            }
            content={
              <table className="form-table">
                <thead>
                  <tr>
                    <td>Name</td>
                    <td className="right">
                      <IconButton label="Member" icon="add" onClick={this.addMember} />
                    </td>
                  </tr>
                </thead>
                <tbody>
                  {this.state.members.map((member, index) => { return <tr key={index}><td>{member}</td></tr> })}
                </tbody>
              </table>
            } />
        </div>
      </div>
    )
  }
}

export default Configuration;