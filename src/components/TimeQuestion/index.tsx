import React, { PureComponent } from 'react';
import './index.css'
import Grid from '../Grid';
import Paging from '../Paging';
import Clock from '../Clock';

interface State {
    isEasyQuestions: boolean
}

class TimeQuestion extends PureComponent<any, {}> {
    public state: State = {
        isEasyQuestions: false
    }

    private options: React.RefObject<Grid> = React.createRef();
    private clock: React.RefObject<Clock> = React.createRef();

    constructor(props: any) {
        super(props);
    }

    componentDidMount() {

    }

    private getEasyAnswers() {
        return [
            { custom: <div className="time-question-option">Two O'Clock</div> },
            { custom: <div className="time-question-option">One O'Clock</div> },
            { custom: <div className="time-question-option">Twelve O'Clock</div> },
        ];
    }

    private getHardAnswers() {
        return [
            { custom: <div className="time-question-option">Six fifteen</div> },
            { custom: <div className="time-question-option">half past six</div> },
            { custom: <div className="time-question-option">Quarter to seven</div> },
        ];
    }

    setQuestion(hour: number, minute: number, second: number, isEasyQuestions: boolean = true) {
        this.options.current!.clear();
        this.clock.current!.setTime(hour, minute, second);
        this.options.current!.addAll(isEasyQuestions ? this.getEasyAnswers() : this.getHardAnswers());
    }

    render() {
        return (
            <div className="time-question-container">
                <div className="form-title">
                    Question
                    <span style={{ float: "right" }}>( 1 / 10 )</span>
                </div>
                <div className="time-question-wrapper">
                    <Clock ref={this.clock} />
                    <div className="time-question-title">What is the time?</div>
                </div>
                <div className="time-question-options-frame">
                    <Grid ref={this.options} handler={this} cardStyle={{ width: "90px", height: "90px", borderRadius: "100%" }} />
                </div>
                <Paging />
            </div>
        )
    }
}

export default TimeQuestion;