import React, { PureComponent } from 'react';
import './index.css'
import Paper from '../Paper';
import IconButton from '../IconButton';
import { Multiplication, TimeQuestionType } from '../Types';
import { isNumber } from 'util';
import Speech from '../Speech';
import { timingSafeEqual } from 'crypto';
import TimeQuestion from '../TimeQuestion';

interface State {
  easyQuestion: Multiplication[],
  hardQuestion: Multiplication[],
  easyTimeQuestion: TimeQuestionType[],
  hardTimeQuestion: TimeQuestionType[]
}

class QuestionPrep extends PureComponent<any, State> {
  public state: State = {
    easyQuestion: new Array<Multiplication>(),
    hardQuestion: new Array<Multiplication>(),
    easyTimeQuestion: new Array<TimeQuestionType>(),
    hardTimeQuestion: new Array<TimeQuestionType>(),
  }
  constructor(props: any) {
    super(props);
    Speech.speak("Questions tab is opened.");
  }

  addEasyQuestion = () => {
    let multiplier: any = prompt("Please enter the multiplier:");
    if (!multiplier) return;
    let multiplicand: any = prompt("Please enter the multiplicand:");
    if (!multiplicand) return;
    if (!isNumber(parseInt(multiplier)) || !isNumber(parseInt(multiplicand)) || (multiplier * multiplicand) > 100 || (multiplier * multiplicand) < 0) {
      alert("Please enter valid numbers");
      return;
    }
    this.state.easyQuestion.push({ multiplier, multiplicand });
    this.forceUpdate();
  }

  addHardQuestion = () => {
    let multiplier: any = prompt("Please enter the multiplier:");
    if (!multiplier) return;
    let multiplicand: any = prompt("Please enter the expected answer:");
    if (!multiplicand) return;
    if ((multiplicand / multiplier).toString().includes(".") || parseInt(multiplier) > 10 || parseInt(multiplier) <= 0) {
      alert("The answer is not an integer value");
      return;
    }
    this.state.hardQuestion.push({ multiplier, multiplicand });
    this.forceUpdate();
  }

  addEasyTimeQuestion = () => {
    let hour: any = prompt("Please enter the hour:");
    if (parseInt(hour) > 12 || parseInt(hour) < 0) {
      alert("Please enter a valid hour");
      return;
    }
    let ans: string | null = prompt("Please enter the answer:");
    if (!ans || ans.split("").join().length < 1) {
      alert("Please enter a valid answer");
      return;
    }
    this.state.easyTimeQuestion.push({ hour: hour, minute: 0, second: 0, ans: ans });
    this.forceUpdate();
  }

  addHardTimeQuestion = () => {
    let hour: any = prompt("Please enter the hour:");
    if (parseInt(hour) > 12 || parseInt(hour) <= 0) {
      alert("Please enter a valid hour");
      return;
    }
    let minute: any = prompt("Please enter the minute:");
    if (parseInt(minute) > 12 || parseInt(minute) <= 0) {
      alert("Please enter a valid hour");
      return;
    }
    let second: any = prompt("Please enter the second:");
    if (parseInt(second) > 12 || parseInt(second) <= 0) {
      alert("Please enter a valid hour");
      return;
    }
    let ans: string | null = prompt("Please enter the answer:");
    if (!ans || ans.split("").join().length < 1) {
      alert("Please enter a valid answer");
      return;
    }
    this.state.hardTimeQuestion.push({ hour, minute, second, ans });
    this.forceUpdate();
  }

  printTime(time: TimeQuestionType) {
    let str = "";
    str += (time.hour > 9 ? time.hour : "0" + time.hour) + ":";
    str += (time.minute > 9 ? time.minute : "0" + time.minute) + ":";
    str += time.second > 9 ? time.second : "0" + time.second;
    return str;
  }

  render() {
    return (
      <div>
        <div className="form-header">
          <div className="form-title-floating" style={{ color: "black" }}>
            Questions
        </div>
          <div className="form-banner question"></div>
        </div>
        <div className="form-content">
          <div className="form-content-subtitle">Time telling exercises</div>
          <Paper
            title={
              <div>
                <span>Easy</span>
                <span className="right">
                  <IconButton label="Question" icon="add" onClick={this.addEasyTimeQuestion} />
                </span>
              </div>
            }
            content={
              <table className="form-table">
                <thead>
                  <tr>
                    <td>Time</td>
                    <td className="padding-right">Expected Answer</td>
                  </tr>
                </thead>
                <tbody>
                  {
                    this.state.easyTimeQuestion.map((question, index) => {
                      return (
                        <tr key={index}>
                          <td>{`${this.printTime(question)}`}</td>
                          <td className="right">{question.ans}</td>
                        </tr>
                      )
                    })
                  }
                </tbody>
              </table>
            } />
          <br />
          <Paper
            title={
              <div>
                <span>Hard</span>
                <span className="right">
                  <IconButton label="Question" icon="add" onClick={this.addHardTimeQuestion} />
                </span>
              </div>
            }
            content={
              <table className="form-table">
                <thead>
                  <tr>
                    <td>Time</td>
                    <td className="padding-right">Expected Answer</td>
                  </tr>
                </thead>
                <tbody>
                  {
                    this.state.hardTimeQuestion.map((question, index) => {
                      return (
                        <tr key={index}>
                          <td>{`${this.printTime(question)}`}</td>
                          <td className="right">{question.ans}</td>
                        </tr>
                      )
                    })
                  }
                </tbody>
              </table>
            } />
          <br />
          <div className="form-content-subtitle">Multiplication exercises</div>
          <Paper
            title={
              <div>
                <span>Easy</span>
                <span className="right">
                  <IconButton label="Question" icon="add" onClick={this.addEasyQuestion} />
                </span>
              </div>
            }
            content={
              <table className="form-table">
                <thead>
                  <tr>
                    <td className="padding-right">Multiplier</td>
                    <td className="padding-right">Multiplicand</td>
                    <td className="padding-right">Expected Answer</td>
                  </tr>
                </thead>
                <tbody>
                  {
                    this.state.easyQuestion.map((question, index) => {
                      return (
                        <tr key={index}>
                          <td className="padding-right">{question.multiplier}</td>
                          <td className="padding-right">{question.multiplicand}</td>
                          <td className="right">{question.multiplier * parseInt(question.multiplicand.toString())}</td>
                        </tr>
                      )
                    })
                  }
                </tbody>
              </table>
            } />
          <br />
          <Paper
            title={
              <div>
                <span>Hard</span>
                <span className="right">
                  <IconButton label="Question" icon="add" onClick={this.addHardQuestion} />
                </span>
              </div>
            }
            content={
              <table className="form-table">
                <thead>
                  <tr>
                    <td className="padding-right">Multiplier</td>
                    <td className="padding-right">Multiplicand</td>
                    <td className="padding-right">Expected Answer</td>
                  </tr>
                </thead>
                <tbody>
                  {
                    this.state.hardQuestion.map((question, index) => {
                      return (
                        <tr key={index}>
                          <td className="padding-right">{question.multiplier}</td>
                          <td className="padding-right">{parseInt(question.multiplicand.toString()) / question.multiplier}</td>
                          <td className="right">{question.multiplicand}</td>
                        </tr>
                      )
                    })
                  }
                </tbody>
              </table>
            } />
        </div>
      </div>
    )
  }
}

export default QuestionPrep;