import React, { PureComponent } from 'react';
import './index.css';

import { Item, LAYOUTS } from '../Types';
import Grid from '../Grid';
import Form from '../Form';
import Training from '../Training';
import Robot from '../Robot';
import Performance from '../Performance';
import Configuration from '../Configuration';
import Question from '../Question';
import QuestionPrep from '../QuestionPrep';
import IconButton from '../IconButton';
import TimeQuestion from '../TimeQuestion';
import Speech from '../Speech';
import Help from '../Help';

interface State {
  questStr: any,
  title: string,
  message: string
}

class Dashboard extends PureComponent<any, {}> {
  public state: State = {
    questStr: null,
    title: "",
    message: ""
  }
  private grid: React.RefObject<Grid> = React.createRef();
  private form: React.RefObject<Form> = React.createRef();
  private questionFormTime: React.RefObject<Form> = React.createRef();
  private questionFormMultiplication: React.RefObject<Form> = React.createRef();
  private questObject: React.RefObject<Question> = React.createRef();
  private timeQuestObject: React.RefObject<TimeQuestion> = React.createRef();
  private helpForm: React.RefObject<Help> = React.createRef();

  private mode: string = "";

  constructor(props: any) {
    super(props);
    this.mode = props.mode;
  }

  componentDidMount() {
    let items = new Array<Item>();
    items.push({ title: "Training", img: "https://cdn.kastatic.org/ka-perseus-images/45a518b64df15c3b0319e2b00b5f6fd4edb7f009.png" });
    items.push({ title: "Performance", img: "https://d3frsattnbx5l6.cloudfront.net/1544957431448-speedometer-speedometer.png" });
    if (this.mode === "teacher") {
      items.push({ title: "Questions", img: "http://pluspng.com/img-png/png-exam-png-ico-512.png" });
      items.push({ title: "Robot Management", img: "https://upload.wikimedia.org/wikipedia/commons/3/38/Robot-clip-art-book-covers-feJCV3-clipart.png" });
      items.push({ title: "Configuration", img: "http://cartonomy.com/wp-content/uploads/2014/10/icon-setup.png" });
    }
    this.grid.current!.addAll(items);
    setTimeout(() => {
      Speech.speak("Dashboard page is opened");
    }, 500)
  }

  launchForm = (id: LAYOUTS) => {
    this.getFormInfo(id);
    this.form.current!.open(id);
  }

  getFormInfo = (id: LAYOUTS) => {
    switch (id) {
      case LAYOUTS.TRAIN:
        this.setState({ title: "Training", message: "Students can train their robot via completing the exercises." });
        break;
      case LAYOUTS.PERFORMANCE:
        this.setState({ title: "Performance", message: "Students can view the performance of their robot here." });
        break;
      case LAYOUTS.QUESTION:
        this.setState({ title: "Question", message: "Teachers can create questions for students to answer." });
        break;
      case LAYOUTS.ROBOT:
        this.setState({ title: "Robot Management", message: "Teachers can view and manage the database of the robot." });
        break;
      case LAYOUTS.CONFIG:
        this.setState({ title: "Configuration", message: "Teachers can assign students to the robot." });
        break;
    }
  }

  launchQuestionFormTime = async (hour: number, minute: number, second: number, isEasyQuestion: boolean = true) => {
    await this.questionFormTime.current!.open();
    this.timeQuestObject.current!.setQuestion(hour, minute, second, isEasyQuestion);
  }

  launchQuestionFormMultiplication = async (questStr: string) => {
    await this.questionFormMultiplication.current!.open();
    this.questObject.current!.setState({ questStr })
  }

  navigateToTimer = () => {
    Speech.speak("Do you want to navigate to the watch application?");
    let ans = confirm("Do you want to navigate to the watch application?");
    if (ans) {
      Speech.speak("Navigating, please wait ...");
      setTimeout(() => window.location.href = "http://www4.comp.polyu.edu.hk/~15011089d/COMP2222/timer", 1000)
    }
  }

  logout = () => {
    Speech.speak("Do you want to logout?");
    let ans = confirm("Do you want to logout?");
    if (ans) {
      Speech.speak("Logging out, please wait ...");
      setTimeout(() => location.reload(), 1000)
    }
  }

  showHelp = () => {
    navigator.vibrate([10]);
    this.helpForm.current!.setState({ title: this.state.title, message: this.state.message }, () => {
      this.helpForm.current!.show();
    })
  }

  hideHelp = () => {    
    this.helpForm.current!.hide();
  }

  render() {
    return (
      <div className="app-container" >
        <div className="base"></div>
        <span className="appTitle">Robot Tutor</span>
        <span className="appExit">
          <IconButton label="Watch" icon="alarm" onClick={() => { navigator.vibrate([10]); this.navigateToTimer() }} />
          <IconButton label="Logout" icon="exit_to_app" onClick={() => { navigator.vibrate([10]); this.logout() }} />
        </span>
        <div className="grid">
          <Grid ref={this.grid} handler={this} onClick={this.launchForm} />
        </div>
        <Form ref={this.form} handler={this} layouts={
          this.mode === "teacher" ?
            [
              <Training handler={this} />,
              <Performance handler={this} />,
              <QuestionPrep handler={this} />,
              <Robot handler={this} />,
              <Configuration handler={this} />
            ] : [
              <Training handler={this} />,
              <Performance handler={this} />,
            ]
        } />
        <Form ref={this.questionFormTime} handler={this} layouts={
          <TimeQuestion ref={this.timeQuestObject} />
        } />
        <Form ref={this.questionFormMultiplication} handler={this} layouts={
          <Question ref={this.questObject} questStr={this.state.questStr} />
        } />
        <Help ref={this.helpForm} handler={this} />
      </div>
    )
  }
}

export default Dashboard;