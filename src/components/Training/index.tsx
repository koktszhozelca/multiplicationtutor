import React, { PureComponent } from 'react';
import './index.css'
import Paper from '../Paper';
import Grid from '../Grid';
import { Item } from '../Types';
import Clock from '../Clock';
import Speech from '../Speech';

class Training extends PureComponent<any, {}> {
  private easyGrid: React.RefObject<Grid> = React.createRef();
  private hardGrid: React.RefObject<Grid> = React.createRef();
  private easyTimeGrid: React.RefObject<Grid> = React.createRef();
  private hardTimeGrid: React.RefObject<Grid> = React.createRef();
  private handler: any = null;

  constructor(props: any) {
    super(props);
    this.handler = props.handler;
    Speech.speak("Training tab is opened.");
  }

  componentDidMount() {
    if (!this.easyGrid.current! || !this.hardGrid.current!) return;
    let easyQuestion: Item[] = new Array<Item>();
    easyQuestion.push(
      { custom: <div className="training-card-icon easy">1</div> },
      { custom: <div className="training-card-icon easy">2</div> },
      { custom: <div className="training-card-icon easy">3</div> },
      { custom: <div className="training-card-icon easy">4</div> },
      { custom: <div className="training-card-icon easy">5</div> },
    );
    this.easyGrid.current!.addAll(easyQuestion);
    this.easyTimeGrid.current!.addAll(easyQuestion);

    let hardQuestion: Item[] = new Array<Item>();
    hardQuestion.push(
      { custom: <div className="training-card-icon hard">1</div> },
      { custom: <div className="training-card-icon hard">2</div> },
      { custom: <div className="training-card-icon hard">3</div> },
      { custom: <div className="training-card-icon hard">4</div> },
      { custom: <div className="training-card-icon hard">5</div> }
    );
    this.hardGrid.current!.addAll(hardQuestion);
    this.hardTimeGrid.current!.addAll(hardQuestion);
  }

  easyTimeQuestion = () => {
    Speech.speak("Opening easy questions for time telling.");
    this.handler.launchQuestionFormTime(2, 0, 5, true);
  }

  hardTimeQuestion = () => {
    Speech.speak("Opening hard questions for time telling.");
    this.handler.launchQuestionFormTime(6, 30, 45, false);
  }

  easyQuestion = () => {
    Speech.speak("Opening easy questions for multiplication.");
    this.handler.launchQuestionFormMultiplication(
      <div>1 x 3 = <div className="training-question-slot">?</div></div>
    )
  }

  hardQuestion = () => {
    Speech.speak("Opening hard questions for multiplication.");
    this.handler.launchQuestionFormMultiplication(
      <div>3 x <div className="training-question-slot">?</div> = 9</div>
    )
  }

  render() {
    return (
      <div>
        <div className="form-header">
          <div className="form-title-floating">
            Training
          </div>
          <div className="form-banner training"></div>
        </div>
        <div className="form-content">
          <div className="form-content-subtitle">Time telling exercises</div>
          <Paper
            title={
              <div>
                <span>Easy</span>
                <span style={{ float: "right" }}>Completed ( 0 / 5 )</span>
              </div>
            }
            content={
              <Grid ref={this.easyTimeGrid} cardStyle={{ width: "100px", height: "100px" }} onClick={this.easyTimeQuestion} />
            } />
          <br />
          <Paper
            title={
              <div>
                <span>Hard</span>
                <span style={{ float: "right" }}>Completed ( 0 / 5 )</span>
              </div>
            }
            content={
              <Grid ref={this.hardTimeGrid} cardStyle={{ width: "100px", height: "100px" }} onClick={this.hardTimeQuestion} />
            } />
        </div>
        <div className="form-content">
        <div className="form-content-subtitle">Multiplication exercises</div>
          <Paper
            title={
              <div>
                <span>Easy</span>
                <span style={{ float: "right" }}>Completed ( 0 / 5 )</span>
              </div>
            }
            content={
              <Grid ref={this.easyGrid} cardStyle={{ width: "100px", height: "100px" }} onClick={this.easyQuestion} />
            } />
          <br />
          <Paper
            title={
              <div>
                <span>Hard</span>
                <span style={{ float: "right" }}>Completed ( 0 / 5 )</span>
              </div>
            }
            content={
              <Grid ref={this.hardGrid} cardStyle={{ width: "100px", height: "100px" }} onClick={this.hardQuestion} />
            } />
        </div>
      </div>

    )
  }
}

export default Training;