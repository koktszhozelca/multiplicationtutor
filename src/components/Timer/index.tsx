import React, { PureComponent } from 'react';
import './index.css';
import moment from 'moment';
import { EventEmitter } from 'events';

const THRESHOLD = 500;
const TimerMode = ["Time Display", "Stopwatch", "Set Alarm Mode", "Set Time Mode"];

enum TIMER_MAIN_STATE {
    TimeDisplay, StopWatch, SetAlarmMode, SetTimeMode
}

enum TIMER_SUB_STATE {
    Start, Hour, Minute, Second
}

enum PRESS_MODE {
    SHORT, LONG
}

interface State {
    mainState: TIMER_MAIN_STATE,
    subState: TIMER_SUB_STATE,
    hour: string,
    minute: string,
    second: string
}

class Timer extends PureComponent<any, {}> {
    public state: State = {
        mainState: TIMER_MAIN_STATE.TimeDisplay,
        subState: TIMER_SUB_STATE.Start,
        hour: "00",
        minute: "00",
        second: "00"
    }
    private startPressTime: any = null;

    private hour: React.RefObject<HTMLSpanElement> = React.createRef();
    private minute: React.RefObject<HTMLSpanElement> = React.createRef();
    private second: React.RefObject<HTMLSpanElement> = React.createRef();

    private eventHandler: EventEmitter;

    constructor(props: any) {
        super(props);

        //Setup the event handler
        this.eventHandler = new EventEmitter();
        this.eventHandler.on("interrupt", () => {
            this.setState({ subState: TIMER_SUB_STATE.Start }, 
                ()=>this.dismissTimeFunction());
        });
    }

    /*
        [OnPressHandler]
        Record the start pressing time.
    */
    onPressDown = () => {
        this.startPressTime = moment();
    }

    /*
        [OnPressHandler]
        Calculate the duration, and determine the press mode.
        The eventHandler is a async event that 
        responsible for firing an interrupt to exit the set-time-mode. 
        The eventHandler sends the interript if and only if the main 
        state is either SetAlarmMode / SetTimeMode.
    */
    onPressRelease = () => {
        let diff = moment().diff(this.startPressTime);
        let duration = moment.duration(diff).asMilliseconds();
        let pressMode: PRESS_MODE = this.interpret(duration);
        this.mainStateRouter(pressMode);
        if (pressMode === PRESS_MODE.LONG &&
            this.state.mainState >= TIMER_MAIN_STATE.SetAlarmMode)
            this.eventHandler.emit("interrupt");
    }

    /*
        A function to check whether the press duration 
        is longer than the threshold (500 ms).
    */
    interpret = (duration: number) => {
        if (duration >= THRESHOLD) return PRESS_MODE.LONG;
        else return PRESS_MODE.SHORT;
    }

    /*
        Serve the corresponding functions based on the
        press mode.
    */
    mainStateRouter = async (pressMode: PRESS_MODE) => {
        switch (pressMode) {
            case PRESS_MODE.LONG:
                return await this.nextMainState();
            case PRESS_MODE.SHORT:
                await this.subStateRouter();
                await this.toggleStopWatch();
            default: return; //It does not exist.
        }
    }

    /*
        Route to set timeMode if and only if
        the main state is set-time-mode / set-alarm-mode
    */
    subStateRouter = async () => {
        switch (this.state.mainState) {
            case TIMER_MAIN_STATE.SetTimeMode:
            case TIMER_MAIN_STATE.SetAlarmMode:
                return await this.nextSubState();
            default: return; //It does not exist.
        }
    }

    /*
        Switching the mode between time display and stopwatch.
    */
    toggleStopWatch = async () => {
        if (this.state.mainState > TIMER_MAIN_STATE.StopWatch) return;
        return await this.setState({
            mainState:
                this.state.mainState === TIMER_MAIN_STATE.TimeDisplay ?
                    TIMER_MAIN_STATE.StopWatch : TIMER_MAIN_STATE.TimeDisplay
        })
    }

    //Main-STN
    nextMainState = async () => {
        switch (this.state.mainState) {
            case TIMER_MAIN_STATE.TimeDisplay:
                return await this.setState({ mainState: TIMER_MAIN_STATE.StopWatch })
            case TIMER_MAIN_STATE.StopWatch:
                return await this.setState({ mainState: TIMER_MAIN_STATE.SetAlarmMode })
            case TIMER_MAIN_STATE.SetAlarmMode:
                return await this.setState({ mainState: TIMER_MAIN_STATE.SetTimeMode })
            case TIMER_MAIN_STATE.SetTimeMode:
                return await this.setState({ mainState: TIMER_MAIN_STATE.TimeDisplay })
            default: return; //It does not exist.
        }
    }

    //Sub-STN
    nextSubState = async () => {
        switch (this.state.subState) {
            case TIMER_SUB_STATE.Start:
                return await this.setState({ subState: TIMER_SUB_STATE.Hour }, ()=>this.setHour());
            case TIMER_SUB_STATE.Hour:
                return await this.setState({ subState: TIMER_SUB_STATE.Minute }, ()=>this.setMinute());
            case TIMER_SUB_STATE.Minute:
                return await this.setState({ subState: TIMER_SUB_STATE.Second }, ()=>this.setSecond());
            case TIMER_SUB_STATE.Second:
                return await this.setState({ subState: TIMER_SUB_STATE.Start },
                    () => this.dismissTimeFunction());
            default: return; //It does not exist.
        }
    }

    //Related to UIs
    dismissTimeFunction = () => {
        this.hour.current!.classList.remove("flashing");
        this.minute.current!.classList.remove("flashing");
        this.second.current!.classList.remove("flashing");
    }

    setHour = () => {
        this.hour.current!.classList.add("flashing");
        this.minute.current!.classList.remove("flashing");
        this.second.current!.classList.remove("flashing");
    }

    setMinute = () => {
        this.hour.current!.classList.remove("flashing");
        this.minute.current!.classList.add("flashing");
        this.second.current!.classList.remove("flashing");
    }

    setSecond = () => {
        this.hour.current!.classList.remove("flashing");
        this.minute.current!.classList.remove("flashing");
        this.second.current!.classList.add("flashing");
    }

    render() {
        return (
            <div className="timer-container">
                <div className="timer-background">
                    <div className="timer-background-frame">
                        <div className="timer-display">
                            <div className="timer-mode">
                                <hr />
                                {TimerMode[this.state.mainState]}
                                <hr />
                            </div>
                            <div className="timer-time">
                                <span ref={this.hour}>{this.state.hour}</span>
                                :
                                <span ref={this.minute}>{this.state.minute}</span>
                                :
                                <span ref={this.second}>{this.state.second}</span>
                            </div>
                            <div className="timer-button"
                                onMouseDown={this.onPressDown}
                                onMouseUp={this.onPressRelease}></div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Timer;