import React, { PureComponent } from 'react';
import './index.css'

import Dashboard from './Dashboard';
import Login from './Login';

import Speech from './Speech';

interface State {
  element: any,
  loginState: any
}

class App extends PureComponent<any, {}> {
  public state: State = {
    element: <Login handler={this} />,
    loginState: null
  }

  constructor(props: any) {
    super(props);
    this.speech();
  }

  student = () => {
    navigator.vibrate([10])
    let element = <Dashboard mode="student" />;
    Speech.speak("Welcome back, student");
    let loginState = true;
    this.setState({ element, loginState });
  }

  teacher = () => {
    navigator.vibrate([10])
    let element = <Dashboard mode="teacher" />;
    Speech.speak("Welcome back, teacher");
    let loginState = true;
    this.setState({ element, loginState });
  }

  speech = () => {
    if (!this.state.loginState)
      Speech.speak("Welcome to robot tutor, please login below.");
  }

  render() {
    return this.state.element;
  }
}

export default App;