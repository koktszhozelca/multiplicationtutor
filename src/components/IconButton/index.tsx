import React, { PureComponent } from 'react';
import './index.css'

class IconButton extends PureComponent<any, {}> {
    private icon: string = "";
    private label: string = "";
    private onClick: any = null;

    constructor(props: any) {
        super(props);
        this.icon = props.icon;
        this.label = props.label;
        this.onClick = props.onClick;
    }

    render() {
        return (
            <table className="iconbtn-container" onClick={this.onClick}>
                <tbody className="iconbtn-content">
                    <tr>
                        <td className="material-icons">{this.icon}</td>
                        <td>{this.label}</td>
                    </tr>
                </tbody>
            </table>
        )
    }
}

export default IconButton;