import React, { PureComponent } from 'react';
import './index.css'
import Paper from '../Paper';
import IconButton from '../IconButton';
import moment from 'moment';
import Speech from '../Speech';

class Performance extends PureComponent<any, {}> {
  constructor(props: any) {
    super(props);
    Speech.speak("Performance tab is opened.");
  }

  render() {
    return (
      <div>
        <div className="form-header">
          <div className="form-title-floating">
            Performance
        </div>
          <div className="form-banner performance"></div>
        </div>
        <div className="form-content">
          <Paper
            title={
              <div>
                <div>
                  <span>Time telling Result</span>
                  <span style={{ float: "right", color: "green" }}>Execellent</span>
                </div>
                <br />
                <div>
                  <span>Last Evaluation</span>
                  <span style={{ float: "right" }}>{moment().subtract(4, 'day').format("YYYY-MM-DD HH:mm:ss")}</span>
                </div>
              </div>
            }
            content={
              <table className="form-table">
                <thead>
                  <tr>
                    <td>Questions</td>
                    <td className="padding-right">Answered</td>
                    <td className="right">Result</td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>02:30:15</td>
                    <td className="padding-right">Half past two</td>
                    <td className="right">
                      True
                    </td>
                  </tr>
                  <tr>
                    <td>06:45:00</td>
                    <td className="padding-right">Quarter to seven</td>
                    <td className="right">
                      True
                    </td>
                  </tr>
                  <tr>
                    <td>07:00:45</td>
                    <td className="padding-right">Seven O'Clock</td>
                    <td className="right">
                      True
                    </td>
                  </tr>
                  <tr>
                    <td>09:05:40</td>
                    <td className="padding-right">Nine past one</td>
                    <td className="right">
                      True
                    </td>
                  </tr>
                  <tr>
                    <td>05:15:30</td>
                    <td className="padding-right">Quarter past one</td>
                    <td className="right">
                      True
                    </td>
                  </tr>
                </tbody>
              </table>
            } />
          <br />
          <Paper
            title={
              <div>
                <div>
                  <span>Multiplication Result</span>
                  <span style={{ float: "right", color: "green" }}>Good</span>
                </div>
                <br />
                <div>
                  <span>Last Evaluation</span>
                  <span style={{ float: "right" }}>{moment().subtract(1, 'day').format("YYYY-MM-DD HH:mm:ss")}</span>
                </div>
              </div>
            }
            content={
              <table className="form-table">
                <thead>
                  <tr>
                    <td>Questions</td>
                    <td className="padding-right">Answered</td>
                    <td className="right">Result</td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1 x 3 = ?</td>
                    <td className="padding-right">3</td>
                    <td className="right">
                      True
                    </td>
                  </tr>
                  <tr>
                    <td>8 x 7 = ?</td>
                    <td className="padding-right">56</td>
                    <td className="right">
                      True
                    </td>
                  </tr>
                  <tr>
                    <td>9 x ? = 27</td>
                    <td className="padding-right">3</td>
                    <td className="right">
                      True
                    </td>
                  </tr>
                  <tr>
                    <td>3 x ? = 21</td>
                    <td className="padding-right">6</td>
                    <td className="right">
                      False
                    </td>
                  </tr>
                  <tr>
                    <td>? x 9 = 63</td>
                    <td className="padding-right">6</td>
                    <td className="right">
                      False
                    </td>
                  </tr>
                </tbody>
              </table>
            } />
        </div>
      </div>
    )
  }
}

export default Performance;