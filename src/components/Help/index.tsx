import React, { PureComponent } from 'react';
import "./index.css";
import Dashboard from '../Dashboard';
import IconButton from '../IconButton';

interface Props {
    //Props here
    handler: Dashboard
}

interface State {
    //State here
    title: string,
    message: string
}

class Help extends PureComponent<Props, State> {
    private backdrop: React.RefObject<HTMLDivElement> = React.createRef();
    private form: React.RefObject<HTMLDivElement> = React.createRef();

    public state: State = {
        title: "Help form title", message: "This is the message"
    }

    //shouldComponentUpdate(){}
    //componentWillMount(){}
    //componentDidMount(){}
    //componentDidUnmount(){}

    show = () => {
        this.backdrop.current!.classList.replace("invisible", "visible");
        this.form.current!.classList.replace("invisible", "visible");
    }

    hide = () => {
        this.backdrop.current!.classList.replace("visible", "invisible");
        this.form.current!.classList.replace("visible", "invisible");
    }

    render() {
        return (
            <div>
                <div className="help-form-backdrop invisible" ref={this.backdrop}></div>
                <div className="help-form invisible" ref={this.form}>
                    <div className="help-form-header">
                        <span className="material-icons help-form-icon">contact_support</span>
                        <span className="help-form-title">
                            {this.state.title}
                        </span>
                    </div>
                    <hr className="help-form-divider" />
                    <div className="help-form-content">
                        {this.state.message}
                    </div>
                    <div className="help-form-action">
                        <IconButton label="Close" icon="close" onClick={this.hide} />
                    </div>
                </div>
            </div>
        )
    }
}

export default Help;