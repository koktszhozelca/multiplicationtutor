import React, { PureComponent } from 'react';
import './index.css'

interface State {
    content: any,
    title: string
}

class Paper extends PureComponent<any, {}> {
    public state: State = {
        content: null,
        title: ""
    }

    constructor(props: any) {
        super(props);
        this.state.title = props.title;
        this.state.content = props.content;
    }

    componentWillReceiveProps(props: any) {
        this.setState({ title: props.title, content: props.content });
    }

    componentDidMount() {
        // this.setState({ title: this.props.title, content: this.props.content });
    }

    render() {
        return (
            <div className="paper-container">
                <div className="paper-title">{this.state.title}</div>
                <div className="paprt-content">
                    {this.state.content}
                </div>
            </div>
        )
    }
}

export default Paper;