import React, { PureComponent } from 'react';
import './index.css'

class Paging extends PureComponent<any, {}> {
    private leftBtn: React.RefObject<HTMLInputElement> = React.createRef();

    constructor(props: any) {
        super(props);
    }

    componentDidMount() {
        // this.leftBtn.current!.before("<")
    }

    render() {
        return (
            <div className="paging-container">
                <div className="paging-button left">
                    <span className="material-icons">keyboard_arrow_left</span>
                </div>
                <div className="paging-button right">
                    <span className="material-icons">keyboard_arrow_right</span>
                </div>
            </div>
        )
    }
}

export default Paging;