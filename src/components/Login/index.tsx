import React, { PureComponent } from 'react';
import './index.css'

class Login extends PureComponent<any, {}> {
    private handler: any = null;
    private form: React.RefObject<HTMLDivElement> = React.createRef();
    private email: React.RefObject<HTMLInputElement> = React.createRef();
    private password: React.RefObject<HTMLInputElement> = React.createRef();

    constructor(props: any) {
        super(props);
        this.handler = props.handler;
    }

    login = () => {
        let email: string = this.getEmail();
        if (email.includes("student")) this.handler.student();
        else if (email.includes("teacher")) this.handler.teacher();
        else this.error();
    }

    getEmail = () => {
        let email = this.email.current!.value;
        if (!email || email.split(" ").join().length === 0) throw this.error();
        return email;
    }

    error = () => {
        navigator.vibrate([100])
        this.form.current!.classList.add("shake");
        setTimeout(() => {
            this.form.current!.classList.remove("shake");
        }, 200);
        throw "Wrong credential error";
    }

    render() {
        return (
            <div className="login-container">
                <div className="login-background"></div>
                <div className="login-form" ref={this.form}>
                    <form>
                        <div className="login-form-banner">
                            <div className="login-form-banner-container">
                                <div className="login-title">Robot Tutor</div>
                            </div>
                        </div>
                        <div className="login-form-controls-container">
                            <div className="login-form-controls">
                                <input type="input" className="login-control" placeholder="Type student / teacher here" ref={this.email} autoFocus/>
                                <input type="password" className="login-control" placeholder="Anything" ref={this.password} />
                            </div>
                            <button type="submit" className="login-control-button" onClick={(e) => { e.preventDefault(); this.login(); }}>Login</button>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

export default Login;