import React, { PureComponent } from 'react';
import './index.css'
import IconButton from '../IconButton';
import Dashboard from '../Dashboard';
import { LAYOUTS } from '../Types';
import Training from '../Training';
import Robot from '../Robot';
import Performance from '../Performance';
import Configuration from '../Configuration';
import Speech from '../Speech';

interface State {
  content: any,
  title: string,
  message: string
}

class Form extends PureComponent<any, {}> {
  public state: State = {
    content: null,
    title: "",
    message: ""
  }

  private form: React.RefObject<HTMLDivElement> = React.createRef();
  private handler: any = null;

  private layouts: any;

  constructor(props: any) {
    super(props);
    this.handler = props.handler;
    this.layouts = props.layouts;
  }

  close = () => {
    navigator.vibrate([10]);
    this.form.current!.classList.remove("scaleIn");
    this.form.current!.classList.add("scaleOut");
  }

  open = async (id: LAYOUTS = 0) => {
    navigator.vibrate([10]);
    this.form.current!.classList.remove("scaleOut");
    this.form.current!.classList.add("scaleIn");
    return await this.setState({ content: (this.layouts instanceof Array) ? this.layouts[id] : this.layouts }, () => {
      return Promise.resolve();
    });
  }

  showHelp = () => {
    this.handler.showHelp(this.state.title, this.state.message);
  }

  render() {
    return (
      <div className="form-container" ref={this.form}>
        <div className="form-header-action">
          <IconButton
            label="Back"
            icon="arrow_back"
            onClick={this.close} />
          <IconButton
            label="What is it?"
            icon="contact_support"
            onClick={this.showHelp} />
        </div>
        <div className="form-content">
          {this.state.content}
        </div>
      </div>
    )
  }
}

export default Form;