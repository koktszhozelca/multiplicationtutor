import React from 'react';

export interface Item {
    title?: string,
    img?: string,
    custom?: any
}

export enum LAYOUTS {
    TRAIN, PERFORMANCE, QUESTION, ROBOT, CONFIG
}

export interface Multiplication {
    multiplier: number,
    multiplicand: number | string
}

export interface TimeQuestionType {
    hour: number,
    minute: number,
    second: number,
    ans: string
}
