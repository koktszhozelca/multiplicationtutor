import React, { PureComponent } from 'react';
import './index.css'

import { Item, LAYOUTS } from '../Types';
import Dashboard from '../Dashboard';

interface State {
    items: Item[]
}

class Grid extends PureComponent<any, {}> {
    private handler: any = null;
    private onClick: any = null;
    private cardStyle: any = null;

    public state: State = {
        items: new Array<Item>()
    }

    constructor(props: any) {
        super(props);
        this.handler = props.handler;
        this.onClick = props.onClick;
        this.cardStyle = props.cardStyle;
    }

    addAll = (items: Item[]) => {
        this.state.items = [...items];
        this.forceUpdate();
    }

    clear = () => {
        this.state.items = [];
        this.forceUpdate();
    }

    select = (id: LAYOUTS) => {
        if (this.onClick)
            this.onClick(id);
    }

    render() {
        return (
            <div className="layout">
                {
                    Object.values(this.state.items).map((item) => {
                        let key = this.state.items.indexOf(item);
                        return (
                            <div key={key} className="card" style={this.cardStyle} onClick={() => this.select(key)}>
                                {
                                    item.title && item.img ?
                                        <div className="content">
                                            <div className="title">{item.title}</div>
                                            <img className="image" src={item.img} />
                                        </div> : <div className="content">{item.custom}</div>
                                }
                            </div>
                        )
                    })
                }
            </div>
        )
    }
}

export default Grid;